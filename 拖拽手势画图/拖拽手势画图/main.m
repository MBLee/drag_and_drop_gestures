//
//  main.m
//  拖拽手势画图
//
//  Created by MB__Lee on 2018/5/8.
//  Copyright © 2018年 MB__Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
