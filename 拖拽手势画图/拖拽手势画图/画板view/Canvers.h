//
//  Canvers.h
//  拖拽手势画图
//
//  Created by MB__Lee on 2018/5/8.
//  Copyright © 2018年 MB__Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Canvers;

@protocol  CanversDelegate<NSObject>

@optional

// 将绘制图形的所有点    在手离开屏幕（绘制结束）时 一次性传递给代理
-(void)canversView:(Canvers *)canver sendAllPoints:(NSArray *)pointArr;

//在绘制过程中将数组中的点   传递给代理----这些点是不断增加的
-(void)canversView:(Canvers *)canver streamSendPoints:(NSMutableArray *)pionts;

//在下一次绘制开始的时候， 让代理清除掉一些自身的渲染效果；
-(void)canversViewBeginPrint:(Canvers *)canver;

@end

@interface Canvers : UIView

// 一遍绘制，一遍给数组里面添加点，，，，，这个数组是动态变化的---调取之后只能获取到某一个状态下点的数量
@property(nonatomic,strong,readonly)NSMutableArray *pointArr;//绘制图形的所有点(CGPoint)
@property(nonatomic,weak)id<CanversDelegate> delegate; //代理接收所有点


@property(nonatomic,assign)BOOL removeAllPoints;//是否删除上次画的东西
@property(nonatomic,strong)UIColor *personalColor;//用户自定义颜色（有默认值：绿色）
@property(nonatomic,assign)CGFloat lineWidth;//用户自定义线宽（有默认值：10）


@end
